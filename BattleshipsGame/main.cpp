#include <iostream>
#include "SeaBattle.h"

int main(int argc, char* argv[]) {

    SeaBattle game;

    game.start();

    return 0;
}
