#pragma once

#include <iostream>

#include <algorithm>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <ctime>
#include <set>
#include <limits>

using namespace std;

// Constants for ANSI escape codes used for terminal formatting
const string ANSI_CLEAR_SCREEN = "\033[2J\033[1;1H";

const string ANSI_RESET_COLOR = "\033[0m";

const string ANSI_BOLD = "\033[1m";

const string ANSI_BLUE = "\033[34m";

const string ANSI_RED = "\033[31m";

const string ANSI_GREEN = "\033[32m";

// Enum to represent the state of a cell in the game grid
enum StateOfCell {
    NONE, SHIP, HIT, MISS
};


// Cell class representing a single cell in the game grid
class Cell {
public:
    //enum StateOfCell { NONE, SHIP, HIT, MISS };

    Cell() : State(NONE) {}

    void setState(StateOfCell newStateOfCell) {
        State = newStateOfCell;
    }

    StateOfCell getState() const {
        return State;
    }


private:
    StateOfCell State;
};


class SeaBattle {
private:
    vector<vector<Cell>> playerGrid;
    vector<vector<Cell>> computerGrid;

    // Sizes of ships that are being placed on the grid
    vector<int> ships = { 5, 4, 3, 2, 2, 1 };

    // Variable storing computer moves, for debuging purposes
    vector<pair<int, int>> computerMoves;

public:

    const int gridSize = 10;

    // Constructor for initializing the game
    SeaBattle() {
        playerGrid = createGrid();
        computerGrid = createGrid();

        srand(static_cast<unsigned int>(time(0)));
    }

    // Get methods used in tests
    vector<vector<Cell>> getPlayerGrid() {
        return playerGrid;
    }

    vector<vector<Cell>> getComputerGrid() {
        return computerGrid;
    }

    // Function to create an empty game grid
    vector<vector<Cell>> createGrid() {
        vector<vector<Cell>> grid;
        for (int i = 0; i < gridSize; ++i) {
            vector<Cell> row;
            for (int j = 0; j < gridSize; ++j) {
                row.push_back(Cell());
            }
            grid.push_back(row);
        }
        return grid;
    }

    // Methods thta
    bool canPlaceShip(const vector<vector<Cell>>& grid, int x, int y, int size, bool horizontal) {
        int dx[] = {-1, 0, 1};
        int dy[] = {-1, 0, 1};

        for (int i = 0; i < size; ++i) {
            int nx = x + (horizontal ? i : 0);
            int ny = y + (horizontal ? 0 : i);

            if (nx < 0 || nx >= gridSize || ny < 0 || ny >= gridSize) {
                return false; 
            }

            for (int xi : dx) {
                for (int yi : dy) {
                    int adjX = nx + xi;
                    int adjY = ny + yi;
                    if (adjX >= 0 && adjX < gridSize && adjY >= 0 && adjY < gridSize && grid[adjY][adjX].getState() != NONE) {
                        return false; 
                    }
                }
            }
        }

       
        if (horizontal) {
            if (x > 0 && grid[y][x - 1].getState() == SHIP) return false;
            if (x + size < gridSize && grid[y][x + size].getState() == SHIP) return false;
        } else {
            if (y > 0 && grid[y - 1][x].getState() == SHIP) return false;
            if (y + size < gridSize && grid[y + size][x].getState() == SHIP) return false;
        }

        return true;
    }

    // Function to place a ship on the grid
    void placeShip(vector<vector<Cell>> &grid, int x, int y, int shipSize, bool horizontal) {
        for (int pos = 0; pos < shipSize; ++pos) {
            int row = y + (horizontal ? 0 : pos);
            int column = x + (horizontal ? pos : 0);
            grid[row][column].setState(SHIP);
        }
    }

    // Displays only players grid, this method is used during deploying the ships
    void showGrid() {
        cout << "  ";
        for (char col = 'A'; col < 'A' + gridSize; col++) {
            cout << col << ' ';
        }
        cout << '\n';
        for (int row = 0; row < gridSize; row++) {
            cout << row << ' ';
            for_each(playerGrid[row].begin(), playerGrid[row].end(), [&](Cell cell) {
                if (cell.getState() == SHIP) {
                    cout << ANSI_BOLD << ANSI_BLUE << 'X' << ' ' << ANSI_RESET_COLOR;
                } else {
                    cout << ANSI_BOLD << ANSI_BLUE << '~' << ' ' << ANSI_RESET_COLOR;
                }
            });
            cout << '\n';
        }
    }

    // This method dispalys both, players and computers grid duriht the gameplay
    void showGrids() {
        cout << "      Your Grid               Computer's Grid" << endl;
        cout << "  ";
        for (char col = 'A'; col < 'A' + gridSize; col++) {
            cout << col << ' ';
        }
        cout << "      ";
        for (char col = 'A'; col < 'A' + gridSize; col++) {
            cout << col << ' ';
        }
        cout << '\n';

        for (int row = 0; row < gridSize; row++) {
            cout << row << ' ';
            for_each(playerGrid[row].begin(), playerGrid[row].end(), [&](Cell cell) {
                char display = '~';
                if (cell.getState() == SHIP) {
                    cout << ANSI_BOLD << ANSI_BLUE << 'X' << ' ' << ANSI_RESET_COLOR;
                } else if (cell.getState() == HIT)  {
                    cout << ANSI_BOLD << ANSI_RED << 'X' << ' ' << ANSI_RESET_COLOR;
                } else if (cell.getState() == MISS) {
                    cout << ANSI_BOLD << ANSI_GREEN << 'o' << ' ' << ANSI_RESET_COLOR;
                } else {
                    cout << ANSI_BOLD << ANSI_BLUE << display << ' ' << ANSI_RESET_COLOR;
                }
            });

            cout << "    " << row << ' ';
            for_each(computerGrid[row].begin(), computerGrid[row].end(), [&](Cell cell) {
                char display = '~';
                if (cell.getState() == HIT) {
                    cout << ANSI_BOLD << ANSI_RED << 'X' << ' ' << ANSI_RESET_COLOR;
                } else if (cell.getState() == MISS) {
                    cout << ANSI_BOLD << ANSI_GREEN << 'o' << ' ' << ANSI_RESET_COLOR;
                } else {
                    cout << ANSI_BOLD << ANSI_BLUE << display << ' ' << ANSI_RESET_COLOR;
                }
            });
            cout << '\n';
        }
    }

   // Function for the computer to randomly deploy ships
    void computerDeployShips() {
        for (int size: ships) {
            bool deployed = false;
            while (!deployed) {
                int x = rand() % gridSize;
                int y = rand() % gridSize;
                bool horizontal = rand() % 2;
                if (canPlaceShip(computerGrid, x, y, size, horizontal)) {
                    for (int i = 0; i < size; i++) {
                        int row = y + (horizontal ? 0 : i);
                        int collumn = x + (horizontal ? i : 0);
                        computerGrid[row][collumn].setState(SHIP);
                    }
                    deployed = true;
                    //cout <<"\n" << x << " " << y << " " << horizontal <<endl;
                }
            }
        }
    }


    // Method that allows player to choose placement of theirs ships
    void playerDeployShips() {
        for (int ship = 0; ship < ships.size(); ++ship) {
            bool previouslyInvalid = false;
            bool deployed = false;
            while (!deployed) {
                clearScreen();
                
                showGrid();

                if (previouslyInvalid) {
                    cout << ANSI_RED << "Invalid placement. Try again.\n" << ANSI_RESET_COLOR;
                }
                cout << "Ship of size " << ships[ship] << ".\n";
                
                auto [x, y, orientation] = getUserInput();

                if (isValidInput(playerGrid, x, y, ships[ship], orientation == 0)) {
                    placeShip(playerGrid, x, y, ships[ship], orientation == 0);
                    previouslyInvalid = false;
                    deployed = true;
                    
                } else {
                    previouslyInvalid = true;
                }
            }
        }
    }



    // Function to get user input for ship placement
    tuple<int, int, int> getUserInput() {
        char x_char;
        int y, orientation;
        do {
            cout << "Enter starting coordinates and orientation: ";
            cin >> x_char;
            if (x_char == '~') { // ~~help comand
                showHelp();
                cin.ignore(numeric_limits<streamsize>::max(), '\n'); 

            } else {
                cin >> y >> orientation;
                

                // Convert the character to uppercase
                x_char = toupper(x_char);

                // Check for invalid input
                if (cin.fail() || x_char < 'A' || x_char >= 'A' + gridSize || y < 0 || y >= gridSize || (orientation != 0 && orientation != 1)) {
                    cin.clear(); // Clear error flag
                    cin.ignore(numeric_limits<streamsize>::max(), '\n'); // Discard invalid input
                    cout << ANSI_RED << "Invalid input. Try again.\n"<<ANSI_RESET_COLOR ;
                } else {
                    break; // Exit the loop if input is valid
                }
            }
        } while (true);
        int x = convertLetterToIndex(x_char);
        return {x, y, orientation};
    }

    // Function to validate user input
    bool isValidInput(const vector<vector<Cell>> &grid, int x, int y, int shipSize, bool horizontal) {
        return !cin.fail() && canPlaceShip(grid, x, y, shipSize, horizontal);
    }

    // Function for the computer to make a move5
    pair<int, int> computerMove() {
        vector<pair<int, int>> unplayedCells;

        for (int y = 0; y < gridSize; ++y) {
            for (int x = 0; x < gridSize; ++x) {
                if (playerGrid[y][x].getState() != HIT && playerGrid[y][x].getState() != MISS) {
                    unplayedCells.emplace_back(x, y);
                }
            }
        }

        if (unplayedCells.empty()) {
            // No valid cells found
            return {-1, -1};  // You can handle this case accordingly in the calling code
        }

        // Randomly choose one of the available cells
        auto cell = next(begin(unplayedCells), rand() % unplayedCells.size());
        computerMoves.push_back(*cell);
        return *cell;
    }


    // Function to handle shooting at a given position on the grid
    bool shoot(vector<vector<Cell>> &grid, int x, int y) {
        if (x < 0 || x >= gridSize || y < 0 || y >= gridSize || grid[y][x].getState() == HIT || grid[y][x].getState() == MISS) {
            cout << (x < 0 || x >= gridSize || y < 0 || y >= gridSize ? "Shot out of bounds!" : "Already shot here!")
                 << endl;
            return false;
        }
        grid[y][x].setState(grid[y][x].getState() == SHIP ? HIT : MISS);

        return grid[y][x].getState() == HIT;
    }


    // Function to check the current state of the game
    bool checkGameState() {
        bool playerFinished = all_of(playerGrid.begin(), playerGrid.end(), [](const vector<Cell> &row) {
            return all_of(row.begin(), row.end(), [](Cell cell) { return cell.getState() != SHIP; });
        });

        bool enemyFinished = all_of(computerGrid.begin(), computerGrid.end(), [](const vector<Cell> &row) {
            return all_of(row.begin(), row.end(), [](Cell cell) { return cell.getState() != SHIP; });
        });

        if (playerFinished || enemyFinished) {
            endGame(playerFinished ? false : true);
            //cout << (playerFinished ? "You won!" : "You lost!") << endl;
        }

        return playerFinished || enemyFinished;
    }


    // Function to start the Sea Battle game
    void start() {
        playerDeployShips();

        computerDeployShips();


        bool playerTurn = true;
        string message = "";
        while (!checkGameState()) {
            clearScreen();

            showGrids();

            char x_char;
            int x, y;

            while (playerTurn) {
                cout << message;
                cout << "Enter coordinates to shoot: ";
                
                cin >> x_char >> y;

                x = convertLetterToIndex(x_char);

                if (cin.fail() || x < 0 || x >= gridSize || y < 0 || y >= gridSize) {
                    cin.clear();
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    //message = ANSI_RED + "Invalid input or shot out of bounds. Try again.\n" + ANSI_RESET_COLOR;
                    cout << ANSI_RED << "Invalid input or shot out of bounds. Try again." << ANSI_RESET_COLOR << endl;
                    message = "";
                } else {
                    clearScreen();
                    StateOfCell prevStateOfCell = computerGrid[y][x].getState();
                    if (shoot(computerGrid, x, y)) {
                        showGrids();
                        message = "You hit!\n";
                        if (checkGameState()) {
                            playerTurn = false;
                        }
                    } else {
                        showGrids();
                        message = "You miss!\n";
                        if (prevStateOfCell != NONE) {
                            message = ANSI_RED + "You already shot here. Shoot again.\n" + ANSI_RESET_COLOR;
                        } else {
                            playerTurn = false;
                        }
                    }
                }
            }

            while (!playerTurn) {
                pair<int, int> computerMoveResult = computerMove();
                int x = computerMoveResult.first;
                int y = computerMoveResult.second;

                if (x == -1 && y == -1) {
                    checkGameState();
                }
                cout << x << y;
                if (shoot(playerGrid, x, y)) {
                    message += "Computer hit your ship.\n";
                } else {
                    message += "Computer missed.\n";
                    playerTurn = true;
                }
                
                checkGameState();
            }

        }
    }

    // Function to display game instructions
    void showHelp() {
        cout << ANSI_BOLD << "\nSea Battle - Instructions" << ANSI_RESET_COLOR << endl;
        cout << "1. You will play against the computer." << endl;
        cout << "2. The goal is to sink all of the computer's ships." << endl;
        cout << "3. You and the computer take turns to shoot at each other's grid." << endl;
        cout << "4. Your grid is on the left, and the computer's grid is on the right." << endl;
        cout << "5. You will be asked to place your ships - Enter coordinates in format:" << endl;
        cout << "    - x coordinate (A - J)" << endl;
        cout << "    - y coordinate (0 - 9)" << endl;
        cout << "    - orientation (0 for horizontal, 1 for vertical)" << endl;
        cout << " For example: B 5 0" << endl;
        cout << "6. Enter coordinates to shoot when it's your turn (x coordinate and y coordinate)" << endl;
        cout << " For example: D6" << endl;
        cout << "7. The grids display the following symbols:" << endl;
        cout << "   - " << ANSI_BOLD << ANSI_BLUE << "~" << ANSI_RESET_COLOR << " : Empty sea" << endl;
        cout << "   - " << ANSI_BOLD << ANSI_BLUE << "X" << ANSI_RESET_COLOR << " : Your ships" << endl;
        cout << "   - " << ANSI_BOLD << ANSI_RED << "X" << ANSI_RESET_COLOR << " : Hit ship" << endl;
        cout << "   - " << ANSI_BOLD << ANSI_GREEN << "o" << ANSI_RESET_COLOR << " : Missed shot" << endl;
        cout << " (Note: Game is not case sensitive)" << endl;
        cout << " Have fun and good luck!\n \n" << endl;
    }

    // Function to convert a letter coordinate to its corresponding index
    int convertLetterToIndex(char letter) {
        return toupper(letter) - 'A';
    }

private:
    // Helper function to clear the terminal screen
    void clearScreen() {
        cout << "\033c"; 
    }

    // Function to end the game and display the result
    void endGame(bool playerWon) {
        cout << "\033c";  // Clear the entire terminal
        cout << (playerWon ? ANSI_GREEN + "You won! \n" : ANSI_RED + "You lost! \n") << ANSI_RESET_COLOR << endl;
        cout << "⠀⠀⠀⠀⠀⠀⢀⣤⠤⠤⠤⠤⠤⠤⠤⠤⠤⠤⢤⣤⣀⣀⡀⠀⠀⠀⠀⠀⠀\n⠀⠀⠀⠀⢀⡼⠋⠀⣀⠄⡂⠍⣀⣒⣒⠂⠀⠬⠤⠤⠬⠍⠉⠝⠲⣄⡀⠀⠀\n⠀⠀⠀⢀⡾⠁⠀⠊⢔⠕⠈⣀⣀⡀⠈⠆⠀⠀⠀⡍⠁⠀⠁⢂⠀⠈⣷⠀⠀\n⠀⠀⣠⣾⠥⠀⠀⣠⢠⣞⣿⣿⣿⣉⠳⣄⠀⠀⣀⣤⣶⣶⣶⡄⠀⠀⣘⢦⡀\n⢀⡞⡍⣠⠞⢋⡛⠶⠤⣤⠴⠚⠀⠈⠙⠁⠀⠀⢹⡏⠁⠀⣀⣠⠤⢤⡕⠱⣷\n⠘⡇⠇⣯⠤⢾⡙⠲⢤⣀⡀⠤⠀⢲⡖⣂⣀⠀⠀⢙⣶⣄⠈⠉⣸⡄⠠⣠⡿\n⠀⠹⣜⡪⠀⠈⢷⣦⣬⣏⠉⠛⠲⣮⣧⣁⣀⣀⠶⠞⢁⣀⣨⢶⢿⣧⠉⡼⠁\n⠀⠀⠈⢷⡀⠀⠀⠳⣌⡟⠻⠷⣶⣧⣀⣀⣹⣉⣉⣿⣉⣉⣇⣼⣾⣿⠀⡇⠀\n⠀⠀⠀⠈⢳⡄⠀⠀⠘⠳⣄⡀⡼⠈⠉⠛⡿⠿⠿⡿⠿⣿⢿⣿⣿⡇⠀⡇⠀\n⠀⠀⠀⠀⠀⠙⢦⣕⠠⣒⠌⡙⠓⠶⠤⣤⣧⣀⣸⣇⣴⣧⠾⠾⠋⠀⠀⡇⠀\n⠀⠀⠀⠀⠀⠀⠀⠈⠙⠶⣭⣒⠩⠖⢠⣤⠄⠀⠀⠀⠀⠀⠠⠔⠁⡰⠀⣧⠀\n⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠛⠲⢤⣀⣀⠉⠉⠀⠀⠀⠀⠀⠁⠀⣠⠏⠀\n⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠉⠉⠛⠒⠲⠶⠤⠴⠒⠚⠁⠀⠀\n" ;

    }

};
