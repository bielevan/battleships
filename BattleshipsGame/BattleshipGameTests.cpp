#include <gtest/gtest.h>
#include "SeaBattle.h"

TEST(SeaBattleTest, CreateGrid) {
    SeaBattle seaBattle;
    auto grid = seaBattle.createGrid();
    ASSERT_EQ(grid.size(), 10);
    for (const auto& row : grid) {
        ASSERT_EQ(row.size(), 10);
        for (const auto& cell : row) {
            ASSERT_EQ(cell.getState(), NONE);
        }
    }
}

TEST(SeaBattleTest, CanPlaceShip) {
    SeaBattle seaBattle;
    auto grid = seaBattle.createGrid();

    ASSERT_TRUE(seaBattle.canPlaceShip(grid, 0, 0, 5, true));
    ASSERT_TRUE(seaBattle.canPlaceShip(grid, 0, 0, 5, false));

    ASSERT_FALSE(seaBattle.canPlaceShip(grid, 8, 0, 5, true));
    ASSERT_FALSE(seaBattle.canPlaceShip(grid, 0, 8, 5, false));
}

TEST(SeaBattleTest, CheckGameState) {
    SeaBattle seaBattle;
    auto grid = seaBattle.createGrid();

    // Simulate sinking all ships in playerGrid
    for (auto& row : grid) {
        for (auto& cell : row) {
            cell.setState(SHIP);
        }
    }

    ASSERT_TRUE(seaBattle.checkGameState());
}



TEST(SeaBattleTest, CheckGameStateEnemyWin) {
    SeaBattle seaBattle;
    auto grid = seaBattle.createGrid();

    // Simulate sinking all ships in playerGrid
    for (auto& row : grid) {
        for (auto& cell : row) {
            cell.setState(SHIP);
        }
    }

    // Check if enemy wins when all player ships are sunk
    ASSERT_TRUE(seaBattle.checkGameState());
}

TEST(SeaBattleTest, ConvertLetterToIndex) {
    SeaBattle seaBattle;

    // Check if the conversion from letter to index is correct
    ASSERT_EQ(seaBattle.convertLetterToIndex('A'), 0);
    ASSERT_EQ(seaBattle.convertLetterToIndex('B'), 1);
    ASSERT_EQ(seaBattle.convertLetterToIndex('C'), 2);
}


int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
