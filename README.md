# Sea Battle - Battleship Game Documentation

## 🌊Overview🌊

Sea Battle is a classic battleship game implemented in C++. The game allows the player to engage in a naval battle against the computer. The objective is to strategically deploy ships and take turns firing shots at the opponent's grid to sink their fleet.

## 🌊Code Structure🌊

### Enums
- `StateOfCell`: Represents the possible states of a cell on the game grid (`NONE`, `SHIP`, `HIT`, `MISS`).

### Classes
- `Cell`: Represents a single cell in the game grid. Each cell maintains its state (`StateOfCell`).

- `SeaBattle`: The main class that orchestrates the game. It includes methods for grid creation, ship placement, shooting, and checking the game state. Additionally, it features ANSI escape codes for colorful console output.

## 🌊Gameplay🌊

1. The game starts with the player strategically placing ships on their grid.
2. The computer also deploys its fleet randomly on its grid.
3. Players take turns to shoot at the opponent's grid by specifying coordinates.
4. The game continues until one player sinks all of the opponent's ships.
5. The player is notified of hits, misses, and the game outcome.

## 🌊How to Play🌊

1. Run the program.
2. Follow on-screen instructions to place your ships.
3. Input coordinates to shoot at the opponent's grid during your turn.
4. Pay attention to the grid symbols (`~` for empty sea, `X` for ships, `o` for misses, `X` in red for hits).
5. The game ends when either you or the computer sink all the opponent's ships.

## 🌊Controls🌊

- When placing ships:
  - Enter starting coordinates (e.g., `A` for column, `5` for row).
    - x coordinate (A - J)" << endl;
    - y coordinate (0 - 9)" << endl;
    - orientation (0 for horizontal, 1 for vertical)" << endl;
   For example: B 5 0" << endl;
  - Type `~~help` to display game instructions.

- During gameplay:
  - Enter coordinates to shoot (e.g., `D6`).

## 🌊Testing🌊

The Sea Battle game undergoes thorough unit testing using the Google Test framework to ensure the accuracy of its core functionalities. 
1. `CreateGrid` test validates the proper initialization of the game grid, checking its dimensions and cell states. 
2. `CanPlaceShip` test evaluates the ship placement logic, confirming the correct determination of valid ship placements. 
3. `CheckGameState` and `CheckGameStateEnemyWin` tests assess the accuracy of the game state-checking mechanism.
4. `ConvertLetterToIndex` test ensures the precise conversion of grid column letters to corresponding indices. 

## 🌊Additional Information🌊

- The game features ANSI escape codes for color and formatting in the console.
- Case sensitivity is not enforced for input.
- The game provides a help command (`~~help`) to display instructions during gameplay.

## Have fun and good luck in the naval battle! 🚢💥

